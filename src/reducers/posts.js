import { ADD_POST_SUCCESS, REQUEST_POSTS, FETCH_POSTS_SUCCESS, DELETE_POST_SUCCESS } from '../actions/PostActions'

const post = (state, action) => {
  switch (action.type) {
    case ADD_POST_SUCCESS:
      return {
        id: action.postBody.id,
        title: action.postBody.title,
        body: action.postBody.body,
        username: action.postBody.username
      }
    default:
      return state
  }
}

const posts = (state = { posts: [] }, action) => {
  switch (action.type) {
    case REQUEST_POSTS:
      return state
    case FETCH_POSTS_SUCCESS:
      return { ...state, posts: action.posts }
    case ADD_POST_SUCCESS:
      return { ...state, posts: [...state.posts, post(undefined, action)] }
    case DELETE_POST_SUCCESS:
      console.log(111222)
        console.log(state)
      return { ...state, posts: state.posts.filter(element => element.id !== action.id) }
    default:
      return state
  }
}

export default posts
