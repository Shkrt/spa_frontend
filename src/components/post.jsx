import React from 'react'

class Post extends React.Component {
  render(){
    const { name, description, onClick } = this.props
    return(
  <div>
    <h2>{name}</h2>
    <p>{description}</p>
    <button onClick={onClick}>Delete</button>
  </div>)
  }
}

Post.propTypes = {
  name: React.PropTypes.string.isRequired,
  description: React.PropTypes.string
}

export default Post
