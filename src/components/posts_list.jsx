import React from 'react'
import PostRow from '../containers/post_row.jsx'

class PostsList extends React.Component {
  render(){
    const { posts, url } = this.props
    return(
      <div>
        {posts.map((post) => (
          <PostRow key={post.id} post={post} url={url}
          />
        ))}
      </div>
   )
  }
}

export default PostsList
