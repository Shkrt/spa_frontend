import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk';
import createLogger from 'redux-logger'
import posts from '../reducers/posts'

const loggerMiddleware = createLogger()

const configureStore = (initialState) => {
  const store = createStore(posts,
                            initialState,
                            applyMiddleware(thunk, loggerMiddleware)
                            )

  if (module.hot) {
    module.hot.accept('../reducers/posts', () => {
      const nextRootReducer = require('../reducers/posts')
      store.replaceReducer(nextRootReducer)
    })
  }

  return store
}

export default configureStore
