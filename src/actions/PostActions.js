//Create new post
export const ADD_POST_REQUEST = 'ADD_POST_REQUEST';
export const ADD_POST_SUCCESS = 'ADD_POST_SUCCESS';
export const REQUEST_POSTS = 'REQUEST_POSTS';
export const FETCH_POSTS_SUCCESS = 'FETCH_POSTS_SUCCESS';
export const DELETE_POST_SUCCESS = 'DELETE_POST_SUCCESS';
export const DELETE_POST_REQUEST = 'DELETE_POST_REQUEST';

function addPostRequest() {
  return {
    type: ADD_POST_REQUEST
  }
}

function addPostSuccess(postBody) {
  return {
    type: ADD_POST_SUCCESS,
    postBody
  }
}

function deletePostRequest() {
  return {
    type: DELETE_POST_REQUEST
  }
}

function requestPosts() {
  return {
    type: REQUEST_POSTS
  }
}

function fetchPostsSuccess(json) {
  return {
    type: FETCH_POSTS_SUCCESS,
    posts: json
  }
}

export function deletePostSuccess(id) {
  return {
    type: DELETE_POST_SUCCESS,
    id
  }
}

export const fetchPosts = (url) => dispatch => {
  dispatch(requestPosts());
  return fetch(url)
    .then(response => response.json())
    .then(json =>
        dispatch(fetchPostsSuccess(json))
        )
}

export const addPost = (postBody, url) => dispatch => {
  dispatch(addPostRequest());
  return fetch(url, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(postBody)})
    .then(response => {
      if(response.status == 201) {
        response.json().then(function(data){
          dispatch(addPostSuccess(data))
        })
      }
    })
}

export const deletePost = (id, url) => dispatch => {
  dispatch(deletePostRequest());
  fetch(url, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json'
    }}).then(function(response) {
    if(response.status == 204) {
      dispatch(deletePostSuccess(id))
    }
  })
}
