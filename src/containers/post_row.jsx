import { connect } from 'react-redux';
import Post from '../components/post.jsx'
import { deletePost } from '../actions/PostActions.js'

const mapStateToProps = (state, ownProps) => ({
  description: ownProps.post.body,
  name: ownProps.post.title
})

const mapDispatchToProps = (dispatch, ownProps) => ({
  onClick: () => {
    dispatch(deletePost(ownProps.post.id, ownProps.url + '/' + ownProps.post.id))
  }
})

const PostRow = connect(
  mapStateToProps,
  mapDispatchToProps
)(Post)

export default PostRow
