import React from 'react';
import { addPost } from '../actions/PostActions'
import { connect } from 'react-redux'

class PostForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      body: '',
      username: ''
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();
    const { dispatch } = this.props
    let { body, title, username } = this.state
    dispatch(addPost({body, title, username}, this.props.url))
  }

  handleChange({target}) {
    this.setState({[target.id]: target.value});
  }

  render () {
    return <form onSubmit={this.handleSubmit}>
      <label>Title</label>
      <input id='title' type='text' value={this.state.title} onChange={this.handleChange} />
      <label>Body</label>
      <input id='body' type='text' value={this.state.body} onChange={this.handleChange} />
      <label>Username</label>
      <input id='username' type='text' value={this.state.username} onChange={this.handleChange} />
      <input type='submit' />
      </form>;
  }
}

const mapStateToProps = () => {
  return { }
}

export default connect(mapStateToProps)(PostForm)
