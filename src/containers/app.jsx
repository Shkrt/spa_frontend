import React from 'react';
import PostsList from '../components/posts_list.jsx'
import PostForm from '../containers/post_form.jsx';
import { fetchPosts } from '../actions/PostActions.js'
import { connect } from 'react-redux'

class App extends React.Component {
  componentDidMount() {
    const { dispatch, url } = this.props
    dispatch(fetchPosts(url))
  }

  render(){
    const { posts, url, onDeleteClick } = this.props
    return(
        <div>
          <PostsList posts={posts} onDeleteClick={onDeleteClick} url={url} />
          <PostForm url={url}/>
        </div>
        )}
}

const mapStateToProps = (state) => {
  const { posts } = state
  return {
    posts
  }
}

export default connect(mapStateToProps)(App)
