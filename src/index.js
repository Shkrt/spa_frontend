import React from 'react';
import ReactDOM from 'react-dom';
import App from './containers/app.jsx';
import 'whatwg-fetch';
import configureStore from './store/configureStore'
import { Provider } from 'react-redux'

const store = configureStore()

ReactDOM.render(
  <Provider store={store}>
    <App url={process.env.APP_URL} />
  </Provider>,
  document.getElementById('root')
);
