module.exports = exports = {
  "extends": "eslint:recommended",
  env: {
    'es6': true,
    'browser': true,
    'commonjs': true
  },
  ecmaFeatures: {
    'modules': true,
    "jsx": true
  },
  plugins: [
    'react'
  ],
  "parser": "babel-eslint",
  rules: {
    "no-undef": 0,
    "no-console": 0,
    "quotes": [2, "single"],
    "strict": [2, "never"],
    "react/jsx-uses-react": 2,
    "react/jsx-uses-vars": 2,
    "react/react-in-jsx-scope": 2
  }
};
